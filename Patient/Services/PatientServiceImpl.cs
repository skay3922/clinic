using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Clinic.Common;
using Clinic.Patient;
using Grpc.Core;
using Microsoft.Extensions.Logging;

namespace Patient.Services
{
    public class PatientServiceImpl : PatientService.PatientServiceBase
    {
        private readonly IPatientDbService _patientDbService;
        private readonly ILogger<PatientServiceImpl> _logger;
        
        public PatientServiceImpl(IPatientDbService patientDbService,
            ILogger<PatientServiceImpl> logger)
        {
            _patientDbService = patientDbService;
            _logger = logger;
        }

        public override Task<PatientResponse> CreatePatient(PatientRequest request, ServerCallContext context)
        {
            try
            {
                var patient = _patientDbService.CreatePatient(request.Patient);

                return Task.FromResult(new PatientResponse()
                {
                    Patient = patient
                });
            }
            catch (RpcException e)
            {
                _logger.LogWarning(e.Message);
                throw new RpcException(new Status(e.Status.StatusCode, e.Status.Detail));
            }
        }

        public override Task<InfoResponse> UpdatePatient(PatientRequest request, ServerCallContext context)
        {
            try
            {
                var isUpdate = _patientDbService.UpdatePatient(request.Patient);

                if (isUpdate)
                    return Task.FromResult(new InfoResponse()
                    {
                        Success = true,
                        Message = "Пациент обновлено."
                    });

                return Task.FromResult(new InfoResponse()
                {
                    Success = false,
                    Message = "Ошибка при обновлении."
                });
            }
            catch (RpcException e)
            {
                Console.WriteLine(e);
                throw new RpcException(new Status(e.Status.StatusCode, e.Status.Detail));
            }
        }

        public override async Task<PatientsResponse> GetPatientList(PatientsRequest request, ServerCallContext context)
        {
            try
            {
                var patients = await _patientDbService.GetPatientList(request.Paging);

                return await Task.FromResult(new PatientsResponse()
                {
                    Patients = { patients }
                });
            }
            catch (RpcException e)
            {
                Console.WriteLine(e);
                throw new RpcException(new Status(e.Status.StatusCode, e.Status.Detail));
            }
        }

        public override async Task<PatientResponse> GetPatientById(GetPatientByIdRequest request, ServerCallContext context)
        {
            try
            {
                var patient = await _patientDbService.GetPatientById(request.PatientId);

                return await Task.FromResult(new PatientResponse()
                {
                    Patient = patient
                });
            }
            catch (RpcException e)
            {
                Console.WriteLine(e);
                throw new RpcException(new Status(e.Status.StatusCode, e.Status.Detail));
            }
        }

        public override Task<InfoResponse> DeletePatient(DeletePatientRequest request, ServerCallContext context)
        {
            try
            {
                var isDelete = _patientDbService.DeletePatient(request.PatientId);

                if (isDelete)
                    return Task.FromResult(new InfoResponse()
                    {
                        Success = true,
                        Message = "Пациент удален."
                    });

                return Task.FromResult(new InfoResponse()
                {
                    Success = false,
                    Message = "Ошибка при удалении."
                });
            }
            catch (RpcException e)
            {
                Console.WriteLine(e);
                throw new RpcException(new Status(e.Status.StatusCode, e.Status.Detail));
            }
        }
    }
}
