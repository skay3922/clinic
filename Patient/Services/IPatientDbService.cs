using System.Collections.Generic;
using System.Threading.Tasks;
using Clinic.Common;
using Patient.Domain;

namespace Patient.Services
{
    public interface IPatientDbService
    {
        PatientInfo CreatePatient(PatientInfo patientInfo);
        bool UpdatePatient(PatientInfo patientInfo);
        Task<List<PatientInfo>> GetPatientList(RequestPageInfo paging);
        Task<PatientInfo> GetPatientById(int id);
        bool DeletePatient(int id);
    }
}