using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Clinic.Common;
using Google.Protobuf.WellKnownTypes;
using Microsoft.EntityFrameworkCore;
using Patient.Data;
using Patient.Domain;

namespace Patient.Services
{
    public class PatientDbService : IPatientDbService
    {
        private readonly IRepository<Patients> _patientsRepository;

        public PatientDbService(IRepository<Patients> patientsRepository)
        {
            _patientsRepository = patientsRepository;
        }

        public PatientInfo CreatePatient(PatientInfo patientInfo)
        {
            var patient = new Patients()
            {
                FullName = patientInfo.FullName,
                Gender = patientInfo.Gender,
                AddressLine1 = patientInfo.AddressLine1,
                DateOfBirth = patientInfo.DateOfBirth.ToDateTime(),
                PhoneNumber = patientInfo.PhoneNumber
            };
            
            _patientsRepository.Insert(patient);

            patientInfo.Id = patient.Id;

            return patientInfo;
        }

        public bool UpdatePatient(PatientInfo patientInfo)
        {
            var patient = _patientsRepository.GetById(patientInfo.Id);

            if (patient == null) return false;

            patient.Id = patientInfo.Id;
            patient.FullName = patientInfo.FullName;
            patient.Gender = patientInfo.Gender;
            patient.AddressLine1 = patientInfo.AddressLine1;
            patient.DateOfBirth = patientInfo.DateOfBirth.ToDateTime();
            patient.PhoneNumber = patientInfo.PhoneNumber;
            
            _patientsRepository.Update(patient);

            return true;
        }

        public async Task<List<PatientInfo>> GetPatientList(RequestPageInfo paging)
        {
            var patients = await _patientsRepository.TableNoTracking
                .Skip(paging.Offset)
                .Take(paging.Limit)
                .Select(p => new PatientInfo()
                {
                    Id = p.Id,
                    FullName = p.FullName,
                    Gender = p.Gender,
                    AddressLine1 = p.AddressLine1,
                    DateOfBirth = p.DateOfBirth.ToUniversalTime().ToTimestamp(),
                    PhoneNumber = p.PhoneNumber
                }).ToListAsync();

            return patients;
        }

        public async Task<PatientInfo> GetPatientById(int id)
        {
            var patient = await _patientsRepository.TableNoTracking
                .FirstOrDefaultAsync(x => x.Id == id);

            if (patient == null) return null;

            return new PatientInfo()
            {
                Id = patient.Id,
                FullName = patient.FullName,
                Gender = patient.Gender,
                AddressLine1 = patient.AddressLine1,
                DateOfBirth = patient.DateOfBirth.ToUniversalTime().ToTimestamp(),
                PhoneNumber = patient.PhoneNumber
            };
        }

        public bool DeletePatient(int id)
        {
            try
            {
                var patient = _patientsRepository.GetById(id);

                if (patient == null) return false;

                _patientsRepository.Delete(patient);

                return true;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                return false;
            }
        }
    }
}