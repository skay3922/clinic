# Clinic

Тестовое задание для кандидатов на вакансию BackEnd C# Developer

Разработать backEnd приложения для ведения картотеки пациентов больницы и регистрации посещений.
Что необходимо реализовать в рамках приложения:
просмотр, создание, редактирование и удаление карточки пациента (ФИО, пол, дата рождения, адрес проживания, телефон);
просмотр, создание, редактирование и удаление посещения (дата посещения, ввод типа посещения (первичный, вторичный прием), диагноз).


Backend  –  REST API (.NET Core).


## Getting started

> git clone https://gitlab.com/skay3922/clinic.git

> dotnet build

> cd docker-compose

> docker-compose build

> docker-compose up -d

> cd ..

> dotnet ef database  update --verbose -s Patient\Patient.csproj

> dotnet ef database  update --verbose -s Visits\Visits.csproj

#### На рисунке показано как построен архитектура проекта, связь между микросервисами через grpc proto файлами. База на PostgreSQL.  

![alt text](struct.png)

#### после успешной работы можно переходить по ссылке http://localhost:3080/swagger/index.html на страницу swagger где можно по тестировать API.

#### Или по этой ссылке  http://167.99.240.181:3080/swagger/index.html на тестовом сервере.