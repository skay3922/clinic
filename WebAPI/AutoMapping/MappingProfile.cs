using System;
using System.Linq;
using AutoMapper;
using Clinic.Common;
using Google.Protobuf.WellKnownTypes;
using WebAPI.Models;

namespace WebAPI.AutoMapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<VisitsRegistrationModel, VisitsInfo>()
                .ForMember(d=>d.VisitsDate, o=>o.MapFrom(j=>j.VisitsDate.ToUniversalTime().ToTimestamp()));
            
            CreateMap<VisitsInfo, VisitsRegistrationModel>()
                .ForMember(d=>d.VisitsDate, o=>o.MapFrom(j=>j.VisitsDate.ToDateTime()));

            CreateMap<VisitsModel, VisitsInfo>()
                .ForMember(d=>d.VisitsDate, o=>o.MapFrom(j=>j.VisitsDate.ToUniversalTime().ToTimestamp()));
            
            CreateMap<VisitsInfo, VisitsModel>()
                .ForMember(d=>d.VisitsDate, o=>o.MapFrom(j=>j.VisitsDate.ToDateTime()));
            
            
            CreateMap<PatientsRegistrationModel, PatientInfo>()
                .ForMember(d=>d.DateOfBirth, o=>o.MapFrom(j=>j.DateOfBirth.ToUniversalTime().ToTimestamp()));
            
            CreateMap<PatientInfo, PatientsRegistrationModel>()
                .ForMember(d=>d.DateOfBirth, o=>o.MapFrom(j=>j.DateOfBirth.ToDateTime()));

            CreateMap<PatientsModel, PatientInfo>()
                .ForMember(d=>d.DateOfBirth, o=>o.MapFrom(j=>j.DateOfBirth.ToUniversalTime().ToTimestamp()));
            
            CreateMap<PatientInfo, PatientsModel>()
                .ForMember(d=>d.DateOfBirth, o=>o.MapFrom(j=>j.DateOfBirth.ToDateTime()));
        }
    }
}