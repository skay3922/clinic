using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Clinic.Common;
using Clinic.Patient;
using Grpc.Core;
using Grpc.Net.Client;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PatientController : Controller
    {
        private readonly IMapper _mapper;

        public PatientController(IMapper mapper)
        {
            _mapper = mapper;
        }

        [HttpPost]
        [Route("create")]
        public async Task<IActionResult> CreatePatient([FromBody] PatientsRegistrationModel patientInfo)
        {
            var channel = GrpcChannel.ForAddress("http://app_patient:30051");
            var client = new PatientService.PatientServiceClient(channel);

            var patient = _mapper.Map<PatientInfo>(patientInfo);

            var reply = await client.CreatePatientAsync(new PatientRequest() { Patient = patient });

            return Ok(reply.Patient);
        }

        [HttpPut]
        [Route("update")]
        public async Task<IActionResult> UpdatePatient([FromBody] PatientsModel patientInfo)
        {
            var channel = GrpcChannel.ForAddress("http://app_patient:30051");
            var client = new PatientService.PatientServiceClient(channel);

            var patient = _mapper.Map<PatientInfo>(patientInfo);

            var reply = await client.UpdatePatientAsync(new PatientRequest() { Patient = patient });

            return Ok(new InfoResponse()
            {
                Success = reply.Success,
                Message = reply.Message
            });
        }

        [HttpGet]
        [Route("list")]
        public async Task<IActionResult> GetPatients(int pageIndex = 0, int pageSize = 30)
        {
            var channel = GrpcChannel.ForAddress("http://app_patient:30051");
            var client = new PatientService.PatientServiceClient(channel);

            var reply = await client.GetPatientListAsync(new PatientsRequest()
            {
                Paging = new RequestPageInfo()
                {
                    Offset = pageIndex,
                    Limit = pageSize
                }
            });
            
            var patients = _mapper.Map<List<PatientsModel>>(reply.Patients);

            return Ok(patients);
        }

        [HttpGet]
        [Route("detail/{id}")]
        public async Task<IActionResult> GetPatient(int id)
        {
            var channel = GrpcChannel.ForAddress("http://app_patient:30051");
            var client = new PatientService.PatientServiceClient(channel);

            var reply = await client.GetPatientByIdAsync(new GetPatientByIdRequest() { PatientId = id });
            
            if (reply.Patient == null)
                return Ok(new InfoResponse() { Success = false, Message = "Пациент не существует или удален." });

            var patient = _mapper.Map<PatientsModel>(reply.Patient);

            return Ok(patient);
        }

        [HttpDelete]
        [Route("delete/{id}")]
        public async Task<IActionResult> DeletePatient(int id)
        {
            var channel = GrpcChannel.ForAddress("http://app_patient:30051");
            var client = new PatientService.PatientServiceClient(channel);

            var reply = await client.DeletePatientAsync(new DeletePatientRequest() { PatientId = id });

            return Ok(new InfoResponse()
            {
                Success = reply.Success,
                Message = reply.Message
            });
        }
    }
}