using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Clinic.Common;
using Clinic.Patient;
using Clinic.Visits;
using Grpc.Core;
using Grpc.Net.Client;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VisitsController : Controller
    {
        private readonly IMapper _mapper;

        public VisitsController(IMapper mapper)
        {
            _mapper = mapper;
        }

        [HttpPost]
        [Route("create")]
        public async Task<IActionResult> CreateVisits([FromBody] VisitsRegistrationModel visitsRegistration)
        {
            var channel = GrpcChannel.ForAddress("http://app_visit:30052");
            var client = new VisitsService.VisitsServiceClient(channel);

            var channelPatient = GrpcChannel.ForAddress("http://app_patient:30051");
            var clientPatient = new PatientService.PatientServiceClient(channelPatient);

            var patientResponse = await clientPatient.GetPatientByIdAsync(new GetPatientByIdRequest()
                { PatientId = visitsRegistration.PatientId });

            if (patientResponse.Patient == null)
                return BadRequest(new InfoResponse()
                {
                    Success = false,
                    Message = "Такого пациента нет."
                });


            var visits = _mapper.Map<VisitsInfo>(visitsRegistration);

            var reply = await client.CreateVisitsAsync(new VisitsRequest() { Visits = visits });

            return Ok(reply.Visits);
        }

        [HttpPut]
        [Route("update")]
        public async Task<IActionResult> UpdateVisits([FromBody] VisitsModel visitsModel)
        {
            var channel = GrpcChannel.ForAddress("http://app_visit:30052");
            var client = new VisitsService.VisitsServiceClient(channel);

            var visits = _mapper.Map<VisitsInfo>(visitsModel);

            var reply = await client.UpdateVisitsAsync(new VisitsRequest() { Visits = visits });

            return Ok(new InfoResponse()
            {
                Success = reply.Success,
                Message = reply.Message
            });
        }

        [HttpGet]
        [Route("list")]
        public async Task<IActionResult> GetVisitsList(int pageIndex = 0, int pageSize = 30)
        {
            var channel = GrpcChannel.ForAddress("http://app_visit:30052");
            var client = new VisitsService.VisitsServiceClient(channel);

            var reply = await client.GetVisitsListAsync(new VisitsListRequest()
            {
                Paging = new RequestPageInfo()
                {
                    Offset = pageIndex,
                    Limit = pageSize
                }
            });
            
            var visits = _mapper.Map<List<VisitsModel>>(reply.Visits);
            
            return Ok(visits);
        }

        [HttpGet]
        [Route("detail/{id}")]
        public async Task<IActionResult> GetVisits(int id)
        {
            var channel = GrpcChannel.ForAddress("http://app_visit:30052");
            var client = new VisitsService.VisitsServiceClient(channel);

            var reply = await client.GetVisitsByIdAsync(new GetVisitsByIdRequest() { VisitsId = id });

            if (reply.Visits == null)
                return Ok(new InfoResponse() { Success = false, Message = "Посещения не существует или удален." });

            return Ok(reply.Visits);
        }

        [HttpDelete]
        [Route("delete/{id}")]
        public async Task<IActionResult> DeleteVisits(int id)
        {
            var channel = GrpcChannel.ForAddress("http://app_visit:30052");
            var client = new VisitsService.VisitsServiceClient(channel);

            var reply = await client.DeleteVisitsAsync(new DeleteVisitsRequest() { VisitsId = id });

            return Ok(new InfoResponse()
            {
                Success = reply.Success,
                Message = reply.Message
            });
        }
    }
}