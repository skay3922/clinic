using System;
using System.ComponentModel.DataAnnotations;

namespace WebAPI.Models
{
    public class VisitsModel
    {
        public int Id { get; set; }
        
        [Required(ErrorMessage = "Вы не указали патциента. ")]
        public int PatientId { get; set; }
        
        [Required]
        public DateTime VisitsDate { get; set; }
        
        public string VisitsType { get; set; }
        public string Diagnosis { get; set; }
    }
}