using System;
using System.ComponentModel.DataAnnotations;

namespace WebAPI.Models
{
    public class VisitsRegistrationModel
    {
        [Required(ErrorMessage = "Вы не указали пациента. ")]
        public int PatientId { get; set; }
        
        [Required(ErrorMessage = "Дата посещения не указано.")]
        public DateTime VisitsDate { get; set; }
        
        public string VisitsType { get; set; }
        
        [Required(ErrorMessage = "Диагноз не указано.")]
        public string Diagnosis { get; set; }
    }
}