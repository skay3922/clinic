using System;

namespace WebAPI.Models
{
    public class PatientsRegistrationModel
    {
        public string FullName { get; set; }
        public string Gender { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string AddressLine1 { get; set; }
        public string PhoneNumber { get; set; }
    }
}