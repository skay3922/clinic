using System;
using Visits.Data;

namespace Visits.Domain
{
    public class UserVisits : BaseEntity
    {
        public int PatientId { get; set; }
        public DateTime VisitsDate { get; set; }
        public string VisitsType { get; set; }
        public string Diagnosis { get; set; }
    }
}