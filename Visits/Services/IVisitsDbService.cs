using System.Collections.Generic;
using System.Threading.Tasks;
using Clinic.Common;

namespace Visits.Services
{
    public interface IVisitsDbService
    {
        VisitsInfo CreateVisits(VisitsInfo visitsInfo);
        bool UpdateVisits(VisitsInfo visitsInfo);
        Task<List<VisitsInfo>> GetVisitsList(RequestPageInfo paging);
        Task<VisitsFullInfo> GetVisitsById(int id);
        bool DeleteVisits(int id);
    }
}