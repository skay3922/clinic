using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Clinic.Common;
using Google.Protobuf.WellKnownTypes;
using Microsoft.EntityFrameworkCore;
using Visits.Data;
using Visits.Domain;

namespace Visits.Services
{
    public class VisitsDbService : IVisitsDbService
    {
        private readonly IRepository<UserVisits> _userVisitsRepository;

        public VisitsDbService(IRepository<UserVisits> userVisitsRepository)
        {
            _userVisitsRepository = userVisitsRepository;
        }

        public VisitsInfo CreateVisits(VisitsInfo visitsInfo)
        {
            var visits = new UserVisits()
            {
                PatientId = visitsInfo.PatientId,
                VisitsDate = visitsInfo.VisitsDate.ToDateTime(),
                VisitsType = visitsInfo.VisitsType,
                Diagnosis = visitsInfo.Diagnosis
            };

            _userVisitsRepository.Insert(visits);

            visitsInfo.Id = visits.Id;

            return visitsInfo;
        }

        public bool UpdateVisits(VisitsInfo visitsInfo)
        {
            var visits = _userVisitsRepository.GetById(visitsInfo.Id);

            if (visits == null) return false;

            visits.Id = visitsInfo.Id;
            visits.VisitsDate = visitsInfo.VisitsDate.ToDateTime();
            visits.VisitsType = visitsInfo.VisitsType;
            visits.Diagnosis = visitsInfo.Diagnosis;

            _userVisitsRepository.Update(visits);

            return true;
        }

        public async Task<List<VisitsInfo>> GetVisitsList(RequestPageInfo paging)
        {
            var visits = await _userVisitsRepository.TableNoTracking
                .Skip(paging.Offset)
                .Take(paging.Limit)
                .Select(v => new VisitsInfo()
                {
                    Id = v.Id,
                    PatientId = v.PatientId,
                    VisitsDate = v.VisitsDate.ToUniversalTime().ToTimestamp(),
                    VisitsType = v.VisitsType,
                    Diagnosis = v.Diagnosis
                }).ToListAsync();

            return visits;
        }

        public async Task<VisitsFullInfo> GetVisitsById(int id)
        {
            var visits = await _userVisitsRepository.TableNoTracking
                .FirstOrDefaultAsync(x => x.Id == id);

            if (visits == null) return null;

            return new VisitsFullInfo()
            {
                Id = visits.Id,
                VisitsDate = visits.VisitsDate.ToUniversalTime().ToTimestamp(),
                VisitsType = visits.VisitsType,
                PatientId = visits.PatientId,
                Diagnosis = visits.Diagnosis
            };
        }

        public bool DeleteVisits(int id)
        {
            try
            {
                var visits = _userVisitsRepository.GetById(id);

                if (visits == null) return false;

                _userVisitsRepository.Delete(visits);

                return true;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                return false;
            }
        }
    }
}