using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Clinic.Common;
using Clinic.Patient;
using Clinic.Visits;
using Grpc.Core;
using Grpc.Net.Client;
using Microsoft.Extensions.Logging;

namespace Visits.Services
{
    public class VisitsServiceImpl : VisitsService.VisitsServiceBase
    {
        private readonly IVisitsDbService _visitsDbService;
        private readonly ILogger<VisitsServiceImpl> _logger;

        public VisitsServiceImpl(IVisitsDbService visitsDbService,
            ILogger<VisitsServiceImpl> logger)
        {
            _visitsDbService = visitsDbService;
            _logger = logger;
        }

        public override Task<VisitsResponse> CreateVisits(VisitsRequest request, ServerCallContext context)
        {
            try
            {
                var visits = _visitsDbService.CreateVisits(request.Visits);

                return Task.FromResult(new VisitsResponse()
                {
                    Visits = visits
                });
            }
            catch (RpcException e)
            {
                _logger.LogWarning(e.Message);
                throw new RpcException(new Status(e.Status.StatusCode, e.Status.Detail));
            }
        }

        public override Task<InfoResponse> UpdateVisits(VisitsRequest request, ServerCallContext context)
        {
            try
            {
                var isUpdate = _visitsDbService.UpdateVisits(request.Visits);

                if (isUpdate)
                    return Task.FromResult(new InfoResponse()
                    {
                        Success = true,
                        Message = "Посещения обновлено."
                    });

                return Task.FromResult(new InfoResponse()
                {
                    Success = false,
                    Message = "Ошибка при обновлении."
                });
            }
            catch (RpcException e)
            {
                Console.WriteLine(e);
                throw new RpcException(new Status(e.Status.StatusCode, e.Status.Detail));
            }
        }

        public override async Task<VisitsListResponse> GetVisitsList(VisitsListRequest request,
            ServerCallContext context)
        {
            try
            {
                var visitsList = await _visitsDbService.GetVisitsList(request.Paging);

                return await Task.FromResult(new VisitsListResponse()
                {
                    Visits = { visitsList }
                });
            }
            catch (RpcException e)
            {
                Console.WriteLine(e);
                throw new RpcException(new Status(e.Status.StatusCode, e.Status.Detail));
            }
        }

        public override async Task<GetVisitsByIdResponse> GetVisitsById(GetVisitsByIdRequest request,
            ServerCallContext context)
        {
            try
            {
                var visits = await _visitsDbService.GetVisitsById(request.VisitsId);

                if (visits != null)
                {
                    var channel = GrpcChannel.ForAddress("http://app_patient:30051");
                    var client = new PatientService.PatientServiceClient(channel);
                    
                    var reply = await client.GetPatientByIdAsync(new GetPatientByIdRequest()
                        { PatientId = visits.PatientId });

                    visits.Patient = reply.Patient;
                }

                return await Task.FromResult(new GetVisitsByIdResponse()
                {
                    Visits = visits
                });
            }
            catch (RpcException e)
            {
                Console.WriteLine(e);
                throw new RpcException(new Status(e.Status.StatusCode, e.Status.Detail));
            }
        }

        public override Task<InfoResponse> DeleteVisits(DeleteVisitsRequest request, ServerCallContext context)
        {
            try
            {
                var isDelete = _visitsDbService.DeleteVisits(request.VisitsId);

                if (isDelete)
                    return Task.FromResult(new InfoResponse()
                    {
                        Success = true,
                        Message = "Посещения удален."
                    });

                return Task.FromResult(new InfoResponse()
                {
                    Success = false,
                    Message = "Ошибка при удалении."
                });
            }
            catch (RpcException e)
            {
                Console.WriteLine(e);
                throw new RpcException(new Status(e.Status.StatusCode, e.Status.Detail));
            }
        }
    }
}