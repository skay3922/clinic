using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Visits.Data
{
    public class ClinicContextFactory : IDesignTimeDbContextFactory<ClinicDbContext>
    {
        public ClinicDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<ClinicDbContext>();
            optionsBuilder.UseNpgsql(
                "User ID=postgres;Password=EvfhFkb2fuAG6WSp;Host=localhost;Port=3432;Database=db_main;Pooling=true;");

            return new ClinicDbContext(optionsBuilder.Options);
        }
    }
}